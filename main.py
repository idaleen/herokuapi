from flask import Flask, jsonify, request, render_template
from flask_pymongo import PyMongo
import json 
import os

app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'test'
app.config['MONGO_URI'] = 'mongodb://Daleen:daleen9941@cluster0-shard-00-00-xfdur.mongodb.net:27017,cluster0-shard-00-01-xfdur.mongodb.net:27017,cluster0-shard-00-02-xfdur.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority'


mongo = PyMongo(app)

@app.route('/')
def home_page():
    return render_template("home.html")

@app.route('/pirates', methods=['GET'])
def get_all_pirates():
    
    records = mongo.db.records
    output=[]
    for q in records.find():
         output.append({'name':q['name'], 'age':q['age'], 'isCaptured':q['isCaptured'], 'treasureCoordinates':q['treasureCoordinates']})
    
    #if len(output) == 0:
    #    output=["Oppps. Database is empty"]
    #return jsonify({'result': output})
    return (json.dumps(output))


@app.route('/pirates/<treasureCoordinates>', methods=['GET'])
def get_one_pirate(treasureCoordinates):
    records = mongo.db.records
    i = records.find_one({'treasureCoordinates':treasureCoordinates})
    if i :
        output = {'name':i['name'], 'age':i['age'], 'isCaptured':i['isCaptured'], 'treasureCoordinates':i['treasureCoordinates']}
    else:
        output ='No pirate with these treasure Coordinates were found'
    return jsonify({'result': output})


@app.route('/pirates', methods=['POST'])
def add_pirate():
    records = mongo.db.records

    data = request.get_json()

    new_pirate = 0
    for d in data:
        name = d['name']
        age= d['age']
        isCaptured= d['isCaptured']
        treasureCoordinates = d['treasureCoordinates']
        records.insert({'name': name, 'age': age,
                                   'isCaptured': isCaptured, 'treasureCoordinates':treasureCoordinates})
        new_pirate +=1
        
    #new_pirate = records.find_one({'_id': pirate_id})
    #output = {'name': new_pirate['name'],'age': new_pirate['age'],'isCaptured': new_pirate['isCaptured'],'treasureCoordinates': new_pirate['treasureCoordinates']}

    output= f"Ayyee ! you've added {new_pirate} pirate/s"
    return jsonify({'result': output})



if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(host='0.0.0.0', port=port)

#if __name__ =='__main__':
#    app.run(debug=True)